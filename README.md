I have taken some nice courses over the last few months online. 
Here are the all the sources.
* Abu-Mostafa Caltech , Recommended by Chuck
* Machine Learning for Physicists, 
    * pankajmehtabu , https://github.com/Emergent-Behaviors-in-Biology/mlreview_notebooks

* ManoharSwamyanathan      
* SQL from Coursera
* Aurelien Geron (Book)
   * Hands-On Machine Learning with Scikit-Learn and TensorFlow
* AlexMorozov: Machine Learning Theory, Rutgers        
* PMehta machine learning  : From Princeton data science.
* CS231 : Stanford class
* ConvNet with Keras: Frank Chollet 
* Neural Network:  MNielson



# List of Books I followed

* 2011_Nadkarni_Natural_Language_Processing_Introduction.pdf
* Haykin_simon_Neural_Networks_and_Learning_Machines_2020.pdf
* AlexGray_Statistics_ML_and_Data_mining_in_astronomy.pdf   
* Jake_VanderPlas_Python_Data_Science_Handbook.pdf
* Algorithms_4th_edition_Robert_Sedgewick_and_Kevin_Wayne.pdf
* Jason_Brownlee_XGBoost_with_Python.pdf
* Aston_Zhang_Dive_into_Deep_Learning.pdf                                    
* Joel_Grus_Data_Science_from_scratch.pdf
* BecomingHumanCheatSheets.pdf                                               
* Kamath_slides_Introduction_To_ML_Partial_2.pdf
* Bharat_Ramasundar_Deep_Learning_for_the_Life_Sciences.pdf                  
* Kelleher_Fundamentals_of_Machine_Learning_for_Predictive_analysis.pdf
* Bishop_Pattern_Recognition_And_ML_solution_manual.pdf                      
* Kulkarni_Shivanda_Natural_Language_Processing_Recipes.pdf
* Bishop_Pattern_Recognition_And_ML_solution_official.pdf                    
* MacKay_David_1995_Information_Theory_Inference_and_Learning_Algorithms.pdf
* Bishop_Pattern_Recognition_And_Machine_Learning.pdf                        
* Marc_Mathematics_for_Machine_Learning.pdf
* Bishop_Pattern_Recognition_And_Machine_Learning_2011.pdf                   
* Metis_Natural_Language_Processing_and_Information_Systems.pdf
* Bowles_machine_learning_in_python.pdf                                      
* Pramod_Singh_Learn_TensorFlow2.0.pdf
* Carl_SSPAR_talk_got_a_job_in_Microsoft.pdf                                 
* Raschka_Python_Machine_Learning.pdf
* Chollet_2018_Deep_Learning_with_Python.pdf                                 
* Thomas_Cormen_Intro_to_Algorithms.pdf
* Claus_Wilke_Fundamentals_of_Data_Visualization.pdf                         
* Trevor_Hastie_Elements_of_statistical_learning.pdf
* Conan_Tensorflow_Machine_Learning.pdf                                      
* alppaydin_Intro2_machinelearning_2010.pdf
* Deb_Variational_Monte_Carlo_Technique.pdf                               
* Deep_learning_GoodFellow.pdf                                               
* Efron_conputer_age_statistical_inference.pdf                               
* Gareth_Introduction_to_Statistical_Learning.pdf                           
* Geron_2017_ML_with_skLearn_and_Tensorflow.pdf                            
* tutorials_point_machine_learning_with_python.pdf
* Hallett_Mastering_Spark_for_Data_Science.pdf


# Papers
* 2006_Hinton_Science_reducing_dimensionality_of_data_with_NN.pdf
* 2010_Hogg_Bovy_Fitting_a_model_to_data.pdf
* 2015_LeCun_Nature_Hinton_DeepLearning.pdf
* 2017_Wang_Raj_On_the_origin_of_deep_learning.pdf



