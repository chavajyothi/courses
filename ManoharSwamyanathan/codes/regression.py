#!/usr/bin/env python

import numpy as np
import pylab as pl
import pandas as pd
import sklearn as skl
import sklearn.linear_model as lm
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline


def Visualize(model,X,y):
    pl.scatter(X, y,  color='black')
    pl.plot(X, model.predict(X), color='blue', linewidth=3)
    pl.title('Grade vs Hours Studied')
    pl.ylabel('Test_Grade')
    pl.xlabel('Hours_Studied')
    pl.show()

def LinearRegression():

    fil='../Data/Grade_Set_1.csv'
    df=pd.read_csv(fil)
    #df.plot(kind='scatter', x='Hours_Studied', y='Test_Grade', title='Grade vs Hours Studied')
    #print (df.columns)
    #print (df.corr())
    my_model=lm.LinearRegression()
    X=df.Hours_Studied.values[:,np.newaxis]
    y=df.Test_Grade.values
    my_model.fit(X,y)
    #print ('intercept: ', my_model.intercept_, 'Coefficient: ', my_model.coef_)

    #Visualize(my_model,X,y)

    df['Test_Grade_Pred']=my_model.predict(X);

    df['SST'] = np.square(df['Test_Grade']-df['Test_Grade'].mean())
    df['SSR'] = np.square(df['Test_Grade_Pred']-df['Test_Grade_Pred'].mean())

    print ('Sum of SSR:',df['SSR'].sum())
    print ('Sum of SST:',df['SST'].sum())
    R2=df['SSR'].sum()/df['SST'].sum()
    print ('R squared (manual)=',R2)
    print ('R squared (build in function)=',r2_score(df.Test_Grade_Pred, y))

def Polynomial_Regression():
    fil='../Data/Grade_Set_2.csv'
    df=pd.read_csv(fil)
    X=df.Hours_Studied[:,np.newaxis];
    y=df.Test_Grade
    lr=lm.LinearRegression()
    deg=4;
    my_model=make_pipeline(PolynomialFeatures(deg), lr)
    my_model.fit(X,y)
    Visualize(my_model, X, y)
    #pl.scatter(x, y,  color='black')
    #pl.plot(x, my_model.predict(x), color='green')
    #pl.show()

if __name__=="__main__":
    #LinearRegression()
    Polynomial_Regression()
